const usersCtrl = {};
const Users = require("../models/Users.js");

usersCtrl.getUsers = async (req, res) => {
  const users = await Users.find();
  res.send(users);
};

usersCtrl.postUsers = async (req, res) => {
const newUser = new Users(req.body);
  await newUser.save();
  res.send("Usuario creado");
};

usersCtrl.deleteUsers = async (req, res) => {
  await Users.findByIdAndDelete(req.params.id);
  res.send("Usuario Eliminadao");
};

module.exports = usersCtrl;
