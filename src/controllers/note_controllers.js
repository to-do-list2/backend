const notesCtrl = {};
const Note = require("../models/Note.js");

notesCtrl.getNotes = async (req, res) => {
  const notes = await Note.find();
  res.json(notes);
};

notesCtrl.getNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  res.json(note);
};

notesCtrl.createNotes = async (req, res) => {
  const { title, content, date, author } = req.body;
  const newNote = new Note({
    title,
    content,
    date,
    author
  });
  await newNote.save();
  res.json({ message: "Nota guardada" });
};

notesCtrl.updateNotes = async (req, res) => {
  await Note.findByIdAndUpdate(req.params.id, req.body);

  res.json({ message: "Nota actualizada" });
};

notesCtrl.deleteNotes = async (req, res) => {
 await Note.findByIdAndDelete(req.params.id);

  res.json({ message: "Nota eliminada" });
};

module.exports = notesCtrl;
