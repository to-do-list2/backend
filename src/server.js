require("dotenv").config();
const app = require("./app.js");
require("./database.js");

async function main() {
  await app.listen(app.get("port"));
  console.log("Servidor corriendo en el puerto 4000");
}

main();
